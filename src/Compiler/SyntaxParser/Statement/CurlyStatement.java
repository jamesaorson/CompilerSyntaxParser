package Compiler.SyntaxParser.Statement;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Decl.VarDecl;
import java.util.ArrayList;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: CurlyStatement.java
  **/
public class CurlyStatement extends Statement {
    private ArrayList<VarDecl> varDeclarations;
    private ArrayList<Statement> statementList;
    
    public CurlyStatement() {
        this(new ArrayList<VarDecl>(), new ArrayList<Statement>());
    }
    
    public CurlyStatement(ArrayList<VarDecl> varDeclarations, ArrayList<Statement> statementList) {
        this.varDeclarations = varDeclarations;
        this.statementList = statementList;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Curly Statement {");
        
        printHelper(childIndent, "{");
        for (VarDecl varDecl : this.varDeclarations) {
            varDecl.print(childIndent);
        }
        for (Statement statement : this.statementList) {
            statement.print(childIndent);
        }
        printHelper(childIndent, "}");
        
        printHelper(indent, "}");
    }
}
