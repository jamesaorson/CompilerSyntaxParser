package Compiler.SyntaxParser;

import Compiler.Scanner.CMinusScanner;
import Compiler.Scanner.Token;
import Compiler.Scanner.TokenType;
import Compiler.SyntaxParser.Decl.*;
import Compiler.SyntaxParser.Expression.*;
import Compiler.SyntaxParser.Other.*;
import Compiler.SyntaxParser.Statement.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static Compiler.Scanner.TokenType.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: CMinusSyntaxParser.java
  **/
public class CMinusSyntaxParser {
    private static CMinusScanner scanner;
    private static Program program;
    private static String astString;
    private static Token currentToken;
    private static String inputFile;
    
    public CMinusSyntaxParser(String inputFile) throws IOException {
        this.scanner = new CMinusScanner(inputFile);
        this.inputFile = inputFile;
        this.currentToken = this.scanner.getNextToken();
        this.program = null;
        this.astString = "";
    }
    
    public void writeToOutputFile(String outputFile)
            throws IOException {
        try (BufferedWriter bufferedWriter = 
                new BufferedWriter(new FileWriter(outputFile))) {
            bufferedWriter.write(astString);
        }
        
        System.out.println("Written to output file [" + outputFile + "]:\n"
                           + astString);
    }
    
    public void parse()
            throws IOException {
        parseProgram();
    }
    
    public void print() {
        if (program == null) {
            System.err.println("Call parse() before you call print()");
        } else {
            astString = "";
            this.program.print();
        }
    }
    
    public static void printHelper(int indent, String bodyString) {
        if (indent <= 0) {
            astString += bodyString + "\n";
        } else {
            astString += String.format("%" + indent + "s"
                                       + bodyString + "\n", "");
        }
    }
    
    private static void logParseError() {
        throw new RuntimeException("[" + inputFile + "] Unexpected token `" +
                                   currentToken.getTokenData() +
                                   "` at line " + currentToken.getLineNumber());
    }
    
    private static Token advanceToken()
            throws IOException {
        Token oldToken = currentToken;
        currentToken = scanner.getNextToken();
        
        return oldToken;
    }
    
    private static int getIntFromToken(Token token) {
        return Integer.parseInt(token.getTokenData().toString());
    }
    
    private static boolean matchToken(TokenType tokenType) {
        return currentToken.getTokenType() == tokenType;
    }

    private static Token matchAndAdvanceToken(TokenType tokenType)
            throws IOException {
        if (matchToken(tokenType)) {
            Token returnToken = currentToken;
            advanceToken();
            
            return returnToken;
        } else {
            logParseError();
            return null;
        }
    }
    
    private static boolean isRelop() {
        return matchToken(GREATER_TOKEN) || 
               matchToken(GREATER_EQ_TOKEN) ||
               matchToken(LESS_TOKEN) ||
               matchToken(LESS_EQ_TOKEN) ||
               matchToken(EQUIVALENT_TOKEN) ||
               matchToken(NOT_EQUIVALENT_TOKEN);
    }
    
    /* First set checks */
    private static boolean isStatementFirstSet() {
        return matchToken(SEMICOLON_TOKEN) ||
               matchToken(OPEN_CURLY_TOKEN) ||
               matchToken(WHILE_TOKEN) ||
               matchToken(RETURN_TOKEN) ||
               matchToken(ID_TOKEN) ||
               matchToken(OPEN_PAREN_TOKEN) || 
               matchToken(NUM_TOKEN) ||
               matchToken(IF_TOKEN);
    }
    
    /* Follow set checks */
    private static boolean isAdditiveExpressionPrimeFollowSet() {
        return isRelop() ||
               matchToken(SEMICOLON_TOKEN) ||
               matchToken(CLOSE_PAREN_TOKEN) ||
               matchToken(CLOSE_BRACKET_TOKEN) ||
               matchToken(COMMA_TOKEN) ||
               matchToken(PLUS_TOKEN) ||
               matchToken(MINUS_TOKEN);
    }
    
    private static boolean isArgsFollowSet() {
        return matchToken(CLOSE_PAREN_TOKEN);
    }
    
    private static boolean isExpressionPrimeFollowSet() {
        return matchToken(SEMICOLON_TOKEN) ||
               matchToken(CLOSE_PAREN_TOKEN) ||
               matchToken(CLOSE_BRACKET_TOKEN) ||
               matchToken(COMMA_TOKEN);
    }
    
    private static boolean isExpressionPrimePrimeFollowSet() {
        return isExpressionPrimeFollowSet();
    }
    
    private static boolean isLocalDeclarationsFollowSet() {
        return isStatementFirstSet() || matchToken(CLOSE_CURLY_TOKEN);
    }
    
    private static boolean isParamPrimeFollowSet() {
        return matchToken(CLOSE_PAREN_TOKEN) || matchToken(COMMA_TOKEN);
    }
    
    private static boolean isSimpleExpressionFollowSet() {
        return isExpressionPrimeFollowSet();
    }
    
    private static boolean isStatementListFollowSet() {
        return matchToken(CLOSE_CURLY_TOKEN) || matchToken(ELSE_TOKEN);
    }
    
    private static boolean isTermPrimeFollowSet() {
        return isRelop() ||
               matchToken(SEMICOLON_TOKEN) ||
               matchToken(CLOSE_PAREN_TOKEN) ||
               matchToken(CLOSE_BRACKET_TOKEN) ||
               matchToken(COMMA_TOKEN) ||
               matchToken(PLUS_TOKEN) ||
               matchToken(MINUS_TOKEN);
    }
    
    private static boolean isVarCallFollowSet() {
        return matchToken(PLUS_TOKEN) ||
               matchToken(MINUS_TOKEN) ||
               matchToken(SEMICOLON_TOKEN) ||
               matchToken(CLOSE_PAREN_TOKEN) ||
               matchToken(CLOSE_BRACKET_TOKEN) ||
               matchToken(MULTIPLY_TOKEN) ||
               matchToken(DIVIDE_TOKEN);
    }

    /* Parse routines */
    private static Program parseProgram()
            throws IOException {
        switch (currentToken.getTokenType()) {
            case INT_TOKEN:
            case VOID_TOKEN:
                program = new Program(parseDeclarationList());
                break;
            default: 
                logParseError();
                return null;
        }

        return program;
    }

    private static ArrayList<Decl> parseDeclarationList()
            throws IOException {
        ArrayList<Decl> declarationList = new ArrayList<Decl>();

        switch (currentToken.getTokenType()) {
            case INT_TOKEN:
            case VOID_TOKEN:
                while (matchToken(INT_TOKEN) || matchToken(VOID_TOKEN)) {
                    declarationList.add(parseDeclaration());
                }
                matchAndAdvanceToken(EOF_TOKEN);
                break;
            default: 
                logParseError();
                return null;
        }

        return declarationList;
    }

    private static Decl parseDeclaration()
            throws IOException {
        Decl declaration;
        String id;

        switch (currentToken.getTokenType()) {
            case INT_TOKEN:
                advanceToken();
                id = matchAndAdvanceToken(ID_TOKEN).getTokenData().toString();
                declaration = parseDeclarationPrime("int", id);
                break;
            case VOID_TOKEN:
                advanceToken();
                id = matchAndAdvanceToken(ID_TOKEN).getTokenData().toString();
                declaration = parseFunDeclaration("void", id);
                break;
            default:
                logParseError();
                return null;
        }

        return declaration;
    }

    private static Decl parseDeclarationPrime(String typeSpecifier, String id)
            throws IOException {
        Decl declarationPrime;
        int num;
        
        switch (currentToken.getTokenType()) {
            case SEMICOLON_TOKEN:
                advanceToken();
                declarationPrime = new VarDecl(id);
                break;
            case OPEN_BRACKET_TOKEN:
                advanceToken();
                num = getIntFromToken(matchAndAdvanceToken(NUM_TOKEN));
                matchAndAdvanceToken(CLOSE_BRACKET_TOKEN);
                matchAndAdvanceToken(SEMICOLON_TOKEN);
                declarationPrime = new VarDecl(id, true, num);
                break;
            case OPEN_PAREN_TOKEN:
                declarationPrime = parseFunDeclaration(typeSpecifier, id);
                break;
            default:
                logParseError();
                return null;
        }
        
        return declarationPrime;
    }

    private static VarDecl parseVarDeclaration()
            throws IOException {
        VarDecl varDeclaration;
        String id;
        
        switch (currentToken.getTokenType()){
            case INT_TOKEN:
                advanceToken();
                id = matchAndAdvanceToken(ID_TOKEN).getTokenData().toString();
                varDeclaration = parseVarDeclarationPrime(id);
                break;
            default:
                logParseError();
                return null;
        }
        
        return varDeclaration;
    }
    
    private static VarDecl parseVarDeclarationPrime(String id)
            throws IOException {
        VarDecl varDeclarationPrime;
        int num;
        
        switch (currentToken.getTokenType()){
            case SEMICOLON_TOKEN:
                advanceToken();
                varDeclarationPrime = new VarDecl(id);
                break;
            case OPEN_BRACKET_TOKEN:
                advanceToken();
                num = getIntFromToken(matchAndAdvanceToken(NUM_TOKEN));
                matchAndAdvanceToken(CLOSE_BRACKET_TOKEN);
                varDeclarationPrime = new VarDecl(id, true, num);
                matchAndAdvanceToken(SEMICOLON_TOKEN);
                break;
            default:
                logParseError();
                return null;
        }
        
        return varDeclarationPrime;
    }

    private static FunDecl parseFunDeclaration(String typeSpecifier, String id)
            throws IOException {
        FunDecl funDeclaration;
        ArrayList<Param> params;
        CurlyStatement compoundStatement;
        
        switch (currentToken.getTokenType()) {
            case OPEN_PAREN_TOKEN:
                    advanceToken();
                    params = parseParams();
                    matchAndAdvanceToken(CLOSE_PAREN_TOKEN);
                    compoundStatement = parseCompoundStatement();
                    funDeclaration = new FunDecl(typeSpecifier,
                                                 id,
                                                 params,
                                                 compoundStatement);
                    break;
            default:
                logParseError();
                return null;
        }
        
        return funDeclaration;
    }

    private static ArrayList<Param> parseParams()
            throws IOException {
        ArrayList<Param> params = new ArrayList<Param>();
        
        switch (currentToken.getTokenType()) {
            case INT_TOKEN:
                params.add(parseParam());
                
                while (matchToken(COMMA_TOKEN)) {
                    matchAndAdvanceToken(COMMA_TOKEN);
                    params.add(parseParam());
                }
                break;
            case VOID_TOKEN:
                advanceToken();
                break;
            default:
                logParseError();
                return null;
        }
        
        return params;
    }

    private static Param parseParam()
            throws IOException {
        Param param;
        String id;
        
        switch (currentToken.getTokenType()) {
            case INT_TOKEN:
                advanceToken();
                id = matchAndAdvanceToken(ID_TOKEN).getTokenData().toString();
                param = parseParamPrime(id);
                break;
            default:
                logParseError();
                return null;
        }
        
        return param;
    }
    
    private static Param parseParamPrime(String id)
            throws IOException {
        Param paramPrime;
        
        switch (currentToken.getTokenType()) {
            case OPEN_BRACKET_TOKEN:
                advanceToken();
                matchAndAdvanceToken(CLOSE_BRACKET_TOKEN);
                paramPrime = new Param(id, true);
                break;
            default:
                if (isParamPrimeFollowSet()) {
                    paramPrime = new Param(id, false);
                    break;
                }
                logParseError();
                return null;
        }
        
        return paramPrime;
    }

    private static CurlyStatement parseCompoundStatement()
            throws IOException {
        CurlyStatement compoundStatement;
        ArrayList<VarDecl> localDeclarations;
        ArrayList<Statement> statementList;
        
        switch (currentToken.getTokenType()) {
            case OPEN_CURLY_TOKEN:
                advanceToken();
                localDeclarations = parseLocalDeclarations();
                statementList = parseStatementList();
                matchAndAdvanceToken(CLOSE_CURLY_TOKEN);
                compoundStatement = new CurlyStatement(localDeclarations,
                                                       statementList);
                break;
            default:
                logParseError();
                return null;
        }
        
        return compoundStatement;
    }

    private static ArrayList<VarDecl> parseLocalDeclarations()
            throws IOException {
        ArrayList<VarDecl> localDeclarations = new ArrayList<VarDecl>();
        
        switch (currentToken.getTokenType()) {
            case INT_TOKEN:
                while (matchToken(INT_TOKEN)) {
                    localDeclarations.add(parseVarDeclaration());
                }
                break;
            default:
                if (isLocalDeclarationsFollowSet()) {
                    localDeclarations = new ArrayList<VarDecl>();
                    break;
                } else {
                    logParseError();
                    return null;
                }
        }
        
        return localDeclarations;
    }

    private static ArrayList<Statement> parseStatementList()
            throws IOException {
        ArrayList<Statement> statementList = new ArrayList<Statement>();
        
        while (isStatementFirstSet()) {
            statementList.add(parseStatement());
        }
        if (!isStatementListFollowSet()) {
            logParseError();
        }
        
        return statementList;
    }

    private static Statement parseStatement()
            throws IOException {
        Statement statement;
        
        switch (currentToken.getTokenType()) {
            case SEMICOLON_TOKEN:
            case ID_TOKEN:
            case OPEN_PAREN_TOKEN:
            case NUM_TOKEN:
                statement = parseExpressionStatement();
                break;
            case OPEN_CURLY_TOKEN:
                statement = parseCompoundStatement();
                break;
            case IF_TOKEN:
                statement = parseSelectionStatement();
                break;
            case WHILE_TOKEN:
                statement = parseIterationStatement();
                break;
            case RETURN_TOKEN:
                statement = parseReturnStatement();
                break;
            default:
                logParseError();
                return null;
        }
        
        return statement;
    }

    private static ExpressionStatement parseExpressionStatement()
            throws IOException {
        Expression expression;
        
        switch (currentToken.getTokenType()) {
            case SEMICOLON_TOKEN:
                advanceToken();
                expression = null;
                break;
            case ID_TOKEN:
            case OPEN_PAREN_TOKEN:
            case NUM_TOKEN:
                expression = parseExpression();
                matchAndAdvanceToken(SEMICOLON_TOKEN);
                break;
            default:
                logParseError();
                return null;
        }
        
        return new ExpressionStatement(expression);
    }

    private static IfStatement parseSelectionStatement()
            throws IOException {
        IfStatement selectionStatement;
        Expression ifExpression;
        Statement ifStatement;
        Statement elseStatement;
        
        switch (currentToken.getTokenType()) {
            case IF_TOKEN:
                advanceToken();
                matchAndAdvanceToken(OPEN_PAREN_TOKEN);
                ifExpression = parseExpression();
                matchAndAdvanceToken(CLOSE_PAREN_TOKEN);
                ifStatement = parseStatement();

                if (matchToken(ELSE_TOKEN)) {
                    matchAndAdvanceToken(ELSE_TOKEN);
                    elseStatement = parseStatement();
                } else {
                    elseStatement = null;
                }
                
                selectionStatement = new IfStatement(ifExpression,
                                                     ifStatement,
                                                     elseStatement);
                break;
            default:
                logParseError();
                return null;
        }
        
        return selectionStatement;
    }

    private static WhileStatement parseIterationStatement()
            throws IOException {
        WhileStatement iterationStatement;
        Expression whileExpression;
        Statement whileStatement;
        
        switch (currentToken.getTokenType()) {
            case WHILE_TOKEN:
                advanceToken();
                matchAndAdvanceToken(OPEN_PAREN_TOKEN);
                whileExpression = parseExpression();
                matchAndAdvanceToken(CLOSE_PAREN_TOKEN);
                whileStatement = parseStatement();
                
                iterationStatement = new WhileStatement(whileExpression,
                                                        whileStatement);
                break;
            default:
                logParseError();
                return null;
        }
        
        return iterationStatement;
    }

    private static ReturnStatement parseReturnStatement()
            throws IOException {
        ReturnStatement returnStatement;
        
        switch (currentToken.getTokenType()) {
            case RETURN_TOKEN:
                advanceToken();
                returnStatement = parseReturnStatementPrime();
                break;
            default:
                logParseError();
                return null;
        }
        
        return returnStatement;
    }

    private static ReturnStatement parseReturnStatementPrime()
            throws IOException {
        ReturnStatement returnStatementPrime;
        Expression expression;
        
        switch (currentToken.getTokenType()) {
            case SEMICOLON_TOKEN:
                advanceToken();
                returnStatementPrime = new ReturnStatement();
                break;
            case ID_TOKEN:
            case OPEN_PAREN_TOKEN:
            case NUM_TOKEN:
                expression = parseExpression();
                matchAndAdvanceToken(SEMICOLON_TOKEN);
                returnStatementPrime = new ReturnStatement(expression);
                break;
            default:
                logParseError();
                return null;
        }
        
        return returnStatementPrime;
    }

    private static Expression parseExpression()
            throws IOException {
        Expression expression;
        String id;
        int num;
        
        switch (currentToken.getTokenType()) {
            case ID_TOKEN:
                id = currentToken.getTokenData().toString();
                advanceToken();
                expression = parseExpressionPrime(id);
                break;
            case OPEN_PAREN_TOKEN:
                advanceToken();
                expression = new Expression(true, parseExpression());
                matchAndAdvanceToken(CLOSE_PAREN_TOKEN);
                expression = parseSimpleExpression(expression);
                break;
            case NUM_TOKEN:
                num = getIntFromToken(currentToken);
                expression = new NumExpression(num);
                advanceToken();
                expression = parseSimpleExpression(expression);
                break;
            default:
                logParseError();
                return null;
        }
        
        return expression;
    }

    private static Expression parseExpressionPrime(String id) 
            throws IOException {
        Expression expressionPrime;
        VarExpression varExpression;
        Expression bracketExpression;
        CallExpression callExpression;
        ArrayList<Expression> args;
        
        switch (currentToken.getTokenType()) {
            case OPEN_BRACKET_TOKEN:
                advanceToken();
                bracketExpression = parseExpression();
                matchAndAdvanceToken(CLOSE_BRACKET_TOKEN);
                varExpression = new VarExpression(id, true, bracketExpression);
                expressionPrime = parseExpressionPrimePrime(varExpression);
                break;
            case OPEN_PAREN_TOKEN:
                advanceToken();
                args = parseArgs();
                matchAndAdvanceToken(CLOSE_PAREN_TOKEN);
                callExpression = new CallExpression(id, args);
                expressionPrime = parseSimpleExpression(callExpression);
                break;
            case ASSIGN_TOKEN:
                advanceToken();
                varExpression = new VarExpression(id);
                expressionPrime = new AssignExpression(varExpression,
                                                       parseExpression());
                break;
            case MULTIPLY_TOKEN:
            case DIVIDE_TOKEN:
            case PLUS_TOKEN:
            case MINUS_TOKEN:
            case LESS_TOKEN:
            case LESS_EQ_TOKEN:
            case GREATER_TOKEN:
            case GREATER_EQ_TOKEN:
            case EQUIVALENT_TOKEN:
            case NOT_EQUIVALENT_TOKEN:
                varExpression = new VarExpression(id);
                expressionPrime = parseSimpleExpression(varExpression);
                break;
            case SEMICOLON_TOKEN:
            case CLOSE_PAREN_TOKEN:
            case CLOSE_BRACKET_TOKEN:
            case COMMA_TOKEN:
                expressionPrime = new VarExpression(id);
                break;
            default:
                if (isExpressionPrimeFollowSet()) {
                    expressionPrime = new VarExpression(id);
                    break;
                } else {
                    logParseError();
                    return null;
                }
        }
        
        return expressionPrime;
    }

    private static Expression parseExpressionPrimePrime(VarExpression varExpression)
            throws IOException {
        Expression expression;
        
        switch (currentToken.getTokenType()) {
            case ASSIGN_TOKEN:
               advanceToken();
               expression = new AssignExpression(varExpression,
                                                 parseExpression());
               break;
            case MULTIPLY_TOKEN:
            case DIVIDE_TOKEN:
            case PLUS_TOKEN:
            case MINUS_TOKEN:
            case GREATER_TOKEN:
            case GREATER_EQ_TOKEN:
            case LESS_TOKEN:
            case LESS_EQ_TOKEN:
            case EQUIVALENT_TOKEN:
            case NOT_EQUIVALENT_TOKEN:
                expression = parseSimpleExpression(varExpression);
                break;
            default:
                if (isExpressionPrimePrimeFollowSet()) {
                    expression = varExpression;
                    break;
                }
                logParseError();
                return null;
        }
                
        return expression;
    }

    private static Expression parseSimpleExpression(Expression expression)
            throws IOException {
        Expression simpleExpression;
        Expression leftExpression;
        Expression rightExpression;
        String relop;
        
        switch (currentToken.getTokenType()) {
            case MULTIPLY_TOKEN:
            case DIVIDE_TOKEN:
            case PLUS_TOKEN:
            case MINUS_TOKEN:
                leftExpression = parseAdditiveExpressionPrime(expression);
                
                if (isRelop()){
                    relop = parseRelop();
                    rightExpression = parseAdditiveExpression();
                    simpleExpression = new BinopExpression(leftExpression,
                                                           relop,
                                                           rightExpression);
                }
                else{
                    simpleExpression = leftExpression;
                }
                break;
            case LESS_TOKEN:
            case LESS_EQ_TOKEN:
            case GREATER_TOKEN:
            case GREATER_EQ_TOKEN:
            case EQUIVALENT_TOKEN:
            case NOT_EQUIVALENT_TOKEN:
                leftExpression = expression;
                relop = parseRelop();
                rightExpression = parseAdditiveExpression();
                simpleExpression = new BinopExpression(leftExpression,
                                                       relop,
                                                       rightExpression);
                break;
            default:
                if (isSimpleExpressionFollowSet()) {
                    simpleExpression = expression;
                    break;
                } else {
                    logParseError();
                    return null;
                }
        }
        
        return simpleExpression;
    }

    private static String parseRelop()
            throws IOException {
        String relop;
        
        switch (currentToken.getTokenType()) {
            case GREATER_TOKEN:
                advanceToken();
                relop = ">";
                break;
            case GREATER_EQ_TOKEN:
                advanceToken();
                relop = ">=";
                break;
            case LESS_TOKEN:
                advanceToken();
                relop = "<";
                break;
            case LESS_EQ_TOKEN:
                advanceToken();
                relop = "<=";
                break;
            case EQUIVALENT_TOKEN:
                advanceToken();
                relop = "==";
                break;
            case NOT_EQUIVALENT_TOKEN:
                advanceToken();
                relop = "!=";
                break;
            default:
                logParseError();
                return null;
        }
        
        return relop;
    }

    private static Expression parseAdditiveExpression()
            throws IOException {
        Expression additiveExpression;
        Expression nextExpression;
        String addop;
        
        switch (currentToken.getTokenType()) {
            case OPEN_PAREN_TOKEN:
            case ID_TOKEN:
            case NUM_TOKEN:
                additiveExpression = parseTerm();
                
                while (matchToken(PLUS_TOKEN) || matchToken(MINUS_TOKEN)) {
                    addop = parseAddop();
                    nextExpression = parseTerm();
                    
                    additiveExpression = new BinopExpression(additiveExpression, addop, nextExpression);
                }
                break;
            default:
                logParseError();
                return null;
        }
        
        return additiveExpression;
    }
    
    private static Expression parseAdditiveExpressionPrime(Expression expression)
            throws IOException {
        Expression additiveExpressionPrime;
        Expression nextExpression;
        String addop;
        
        switch (currentToken.getTokenType()) {
            case MULTIPLY_TOKEN:
            case DIVIDE_TOKEN:
                additiveExpressionPrime = parseTermPrime(expression);
                 
                while (matchToken(PLUS_TOKEN) || matchToken(MINUS_TOKEN)) {
                    addop = parseAddop();
                    nextExpression = parseTerm();
                    
                    additiveExpressionPrime = new BinopExpression(additiveExpressionPrime, addop, nextExpression);
                }
                break;
            case PLUS_TOKEN:
            case MINUS_TOKEN:
                additiveExpressionPrime = expression;
                 
                while (matchToken(PLUS_TOKEN) || matchToken(MINUS_TOKEN)) {
                    addop = parseAddop();
                    nextExpression = parseTerm();
                    
                    additiveExpressionPrime = new BinopExpression(additiveExpressionPrime, addop, nextExpression);
                }
                break;
            default:
                if (isAdditiveExpressionPrimeFollowSet()) {
                    additiveExpressionPrime = new BinopExpression(expression);
                } else {
                    logParseError();
                    return null;
                }
                break;
        }
        
        return additiveExpressionPrime;
    }
    
    private static String parseAddop()
            throws IOException {
        String addop;
        
        switch (currentToken.getTokenType()) {
            case PLUS_TOKEN:
                advanceToken();
                addop = "+";
                break;
            case MINUS_TOKEN:
                advanceToken();
                addop = "-";
                break;
            default:
                logParseError();
                return null;
        }
                
        return addop;
    }

    private static Expression parseTerm()
            throws IOException {
        Expression term;
        Expression currentExpression;
        Expression nextExpression;
        String mulop;
        
        switch (currentToken.getTokenType()) {
            case OPEN_PAREN_TOKEN:
            case ID_TOKEN:
            case NUM_TOKEN:
                term = parseFactor();
                
                while (matchToken(MULTIPLY_TOKEN) || matchToken(DIVIDE_TOKEN)) {
                    mulop = parseMulop();
                    nextExpression = parseFactor();
                    
                    term = new BinopExpression(term, mulop, nextExpression);
                }
                break;
            default:
                logParseError();
                return null;
        }
        
        return term;
    }
    
    private static Expression parseTermPrime(Expression expression)
            throws IOException {
        Expression termPrime;
        Expression nextExpression;
        String mulop;
        
        switch (currentToken.getTokenType()) {
            case MULTIPLY_TOKEN:
            case DIVIDE_TOKEN:
                termPrime = expression;
                mulop = parseMulop();
                nextExpression = parseFactor();
                termPrime = new BinopExpression(termPrime,
                                                mulop,
                                                nextExpression);
                
                while (matchToken(MULTIPLY_TOKEN) || matchToken(DIVIDE_TOKEN)) {
                    mulop = parseMulop();
                    nextExpression = parseFactor();
                    
                    termPrime = new BinopExpression(termPrime,
                                                    mulop,
                                                    nextExpression);
                }
                break;
            default:
                if (isTermPrimeFollowSet()) {
                    termPrime = new BinopExpression(expression);
                } else {
                    logParseError();
                    return null;
                }
        }
        
        return termPrime;
    }

    private static String parseMulop()
            throws IOException {
        String mulop;
        
        switch (currentToken.getTokenType()) {
            case MULTIPLY_TOKEN:
                advanceToken();
                mulop = "*";
                break;
            case DIVIDE_TOKEN:
                advanceToken();
                mulop = "/";
                break;
            default:
                logParseError();
                return null;
        }
        
        return mulop;
    }

    private static Expression parseFactor()
            throws IOException {
        Expression factor;
        String id;
        int num;
        
        switch (currentToken.getTokenType()) {
            case OPEN_PAREN_TOKEN:
                advanceToken();
                factor = parseExpression();
                matchAndAdvanceToken(CLOSE_PAREN_TOKEN);
                break;
            case ID_TOKEN:
                id = advanceToken().getTokenData().toString();
                factor = parseVarCall(id);
                break;
            case NUM_TOKEN:
                num = getIntFromToken(advanceToken());
                factor = new NumExpression(num);
                break;
            default:
                logParseError();
                return null;
        }
        
        return factor;
    }

    private static Expression parseVarCall(String id)
            throws IOException {
        Expression varCall;
        Expression bracketExpression;
        ArrayList<Expression> args;
        
        switch (currentToken.getTokenType()) {
            case OPEN_BRACKET_TOKEN:
                advanceToken();
                bracketExpression = parseExpression();
                matchAndAdvanceToken(CLOSE_BRACKET_TOKEN);
                varCall = new VarExpression(id, true, bracketExpression);
                break;
            case OPEN_PAREN_TOKEN:
                advanceToken();
                args = parseArgs();
                matchAndAdvanceToken(CLOSE_PAREN_TOKEN);
                varCall = new CallExpression(id, args);
                break;
            default:
                if (isVarCallFollowSet()) {
                    varCall = new VarExpression(id);
                } else {
                    logParseError();
                    return null;
                }
        }
        
        return varCall;
    }

    private static ArrayList<Expression> parseArgs()
            throws IOException {
        ArrayList<Expression> args;
        
        switch (currentToken.getTokenType()) {
            case OPEN_PAREN_TOKEN:
            case ID_TOKEN:
            case NUM_TOKEN:
                args = parseArgList();
                break;
            default:
                if (isArgsFollowSet()) {
                    args = new ArrayList<Expression>();
                } else {
                    logParseError();
                    return null;
                }
        }
        
        return args;
    }

    private static ArrayList<Expression> parseArgList()
            throws IOException {
        ArrayList<Expression> argList = new ArrayList<Expression>();
        
        switch (currentToken.getTokenType()) {
            case OPEN_PAREN_TOKEN:
            case ID_TOKEN:
            case NUM_TOKEN:
                argList.add(parseExpression());
                
                if (matchToken(COMMA_TOKEN)) {
                    while (matchToken(COMMA_TOKEN)) {
                        matchAndAdvanceToken(COMMA_TOKEN);
                        argList.add(parseExpression());
                    }
                }
                break;
            default:
                logParseError();
                return null;
        }
        
        return argList;
    }
}