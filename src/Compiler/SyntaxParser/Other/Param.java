package Compiler.SyntaxParser.Other;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Param.java
  **/
public class Param {
    private String id;
    private boolean hasBrackets;
    
    public Param() {
        this(null, false);
    }
    
    public Param(String id, boolean hasBrackets) {
        this.id = id;
        this.hasBrackets = hasBrackets;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Param {");
        
        printHelper(childIndent, "int");
        printHelper(childIndent, this.id);
        if (this.hasBrackets) {
            printHelper(childIndent, "[");
            printHelper(childIndent, "]");
        }
        
        printHelper(indent, "}");
    }
}
