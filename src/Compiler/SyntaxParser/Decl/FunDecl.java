package Compiler.SyntaxParser.Decl;

import Compiler.SyntaxParser.Other.Param;
import Compiler.SyntaxParser.Statement.CurlyStatement;
import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import java.util.ArrayList;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: FunDecl.java
  **/
public class FunDecl extends Decl {
    private ArrayList<Param> paramList;
    private CurlyStatement compoundStatement;
    
    public FunDecl() {
        this(null, null, new ArrayList<Param>(), null);
    }
    
    public FunDecl(String typeSpecifier,
                   String id, 
                   ArrayList<Param> paramList, 
                   CurlyStatement compoundStatement) {
        this.typeSpecifier = typeSpecifier;
        this.id = id;
        this.paramList = paramList;
        this.compoundStatement = compoundStatement;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Fun Decl {");
        
        printHelper(childIndent, this.typeSpecifier);
        printHelper(childIndent, this.id);
        printHelper(childIndent, "(");
        if (this.paramList.isEmpty()) {
            printHelper(childIndent, "void");
        } else {
            this.paramList.get(0).print(childIndent);
            for (int i = 1; i < this.paramList.size(); ++i) {
                printHelper(childIndent, ",");
                this.paramList.get(i).print(childIndent);
            }
        }
        printHelper(childIndent, ")");
        this.compoundStatement.print(childIndent);
        
        printHelper(indent, "}");
    }
}
